<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : panier-step2.php
// ceci est la page panier d'achat à la 3e étape -> mode de paiement
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mon Panier</h1>
        </div>
    </section>

    <!-- debut -> stepper -->
    <section id="step" class="container no-visible-sl">
        <div class="stepper-horiz">
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper done active">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>4</span>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper -->

    <!-- debut -> stepper-mobile -->
    <section id="stepper-mobile" class="container no-visible">
        <div class="stepper-vert">
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper done active">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>4</span>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper-mobile -->

    <!-- debut -> box-paiement -->
    <section id="box-paiement" class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div id="mode-paiement">
                    <h1>Veuillez sélectionner le mode de paiement.</h1>
                    <form action="/" name="mode-paiement" method="post">
                        <table class="table">
                            <thead>
                            </thead>
                            <tbody>
                                <!-- debut -> item-mode-paiement -->
                                <tr>
                                    <td class="check-radio">
                                        <input id="maestro" name="paiement_id" value="" checked="checked" type="radio">
                                    </td>
                                    <td class="paiement-img">
                                        <img src="asset/img/Maestro.png" alt="">
                                    </td>
                                    <td class="paiement-txt">
                                        Maestro
                                    </td>
                                </tr>
                                <!-- fin -> item-mode-paiement -->

                                <!-- debut -> item-mode-paiement -->
                                <tr>
                                    <td class="check-radio">
                                        <input id="mastercard" name="paiement_id" value="" type="radio">
                                    </td>
                                    <td class="paiement">
                                        <img src="asset/img/MasterCard.png" alt="">
                                    </td>
                                    <td class="paiement-txt">
                                        Mastercard
                                    </td>
                                </tr>
                                <!-- fin -> item-mode-paiement -->

                                <!-- debut -> item-mode-paiement -->
                                <tr>
                                    <td class="check-radio">
                                        <input id="visa" name="paiement_id" value="" type="radio">
                                    </td>
                                    <td class="paiement">
                                        <img src="asset/img/Visa.png" alt="">
                                    </td>
                                    <td class="paiement-txt">
                                        Visa
                                    </td>
                                </tr>
                                <!-- fin -> item-mode-paiement -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>

            <!-- debut -> card-commande -->
            <div class="col-lg-4 col-md-4">
                <div id="card-commande" class="card sticky">
                    <div class="card-body">
                        <h4 class="card-title">Ma commande</h4>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td colspan="2">Sous-total TTC</td>
                                    <td>3,90 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Taxes</td>
                                    <td>0,22 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Sous-total TTC</td>
                                    <td>3,90 €</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div>
                                <a class="btn btn-success" href="panier-step4.php" role="button">
                                    Finaliser ma commande
                                </a>
                            </div>
                        </li>
                        <li class="list-group-item box-help">
                            <h1>Besoin d'aide ?</h1>
                            <p>
                                Envoyer un mail ou appeler le <br />
                                0479/615 911
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- fin -> card-commande -->
        </div>

        <!-- debut -> btn-shopping -->
        <div class="btn-shopping">
            <div class="row">
                <div class="col-6">
                    <a href="panier-step2.php" class="btn btn-dark btn-retour">
                        Retour
                    </a>
                </div>
                <div class="col-6">
                    <a href="panier-step4.php" class="btn btn-success btn-continuer">
                        Finaliser ma commande
                    </a>
                </div>
            </div>
        </div>
        <!-- fin -> btn-shopping -->

    </section>
    <!-- fin -> box-paiement -->


</main>

<?php include 'footer.php'; ?>
