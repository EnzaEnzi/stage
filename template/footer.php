<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : footer.php
// ceci est le footer de TOUTES les pages
//======================================================================

?>

<!-- debut -> footer -->
<footer id="footer">
    <!-- debut -> pastille -->
    <section class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-12 pastille">
                <div class="pastille-texte">
                    <span>Délice de notre terroir</span>
                    <img src="asset/img/icon/belgium.png" alt="Délice de notre terroir">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-12 pastille">
                <div class="pastille-texte">
                    <span>100% artisanale</span>
                    <img src="asset/img/icon/artisanale.png" alt="100% artisanale">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-12 pastille">
                <div class="pastille-texte">
                    <span>Paiement Sécurisé</span>
                    <img src="asset/img/icon/shield.png" alt="Paiement Sécurisé">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-12 pastille">
                <div class="pastille-texte">
                    <span>Livraison offerte</span>
                    <img src="asset/img/icon/delivery.png" alt="Livraison offerte">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-12 pastille">
                <div class="pastille-texte">
                    <span>Qualité garantie</span>
                    <img src="asset/img/icon/recommended.png" alt="Qualité garantie">
                </div>
            </div>
        </div>
    </section>
    <!-- fin -> pastille -->

    <!-- debut -> info -->
    <section id="info" class="container">
        <div class="row">
            <!-- debut -> paiement -->
            <div id="info-paiement" class="col-lg-3 col-md-3">
                <h1>Mode de paiement</h1>
                <div class="row mode">
                    <div class="col-6">
                        <img src="asset/img/icon/maestro3.png" alt="Maestro">
                    </div>
                    <div class="col-6">
                        <img src="asset/img/icon/mastercard3.png" alt="Mastercard">
                    </div>
                </div>
                <div class="row mode">
                    <div class="col-6">
                        <img src="asset/img/icon/visa1.png" alt="Visa">
                    </div>
                    <!-- <div class="col-6">
                    <img src="asset/img/icon/virement-bancaire.png" alt="Virement bancaire" style="height:28px;">
                </div> -->
            </div>
        </div>
        <!-- fin -> paienement -->
        <span class="footer-separation no-visible-sl"></span>
        <!-- debut -> nav produits footer -->
        <div id="nav-produit" class="col-lg-2 col-md-2">
            <h1>Nos produits</h1>
            <nav>
                <ul>
                    <li>
                        <!-- <i class="fa fa-chevron-right" aria-hidden="true"></i> -->
                        <span class="icons icon-arrow-right"></span>
                        <a href="#">Délice en sachet</a>
                    </li>
                    <li>
                        <span class="icons icon-arrow-right"></span>
                        <a href="#">Délice XL</a>
                    </li>
                    <li>
                        <span class="icons icon-arrow-right"></span>
                        <a href="#">Bonbons Gicopa</a>
                    </li>
                    <li>
                        <span class="icons icon-arrow-right"></span>
                        <a href="#">Délice de saison</a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- fin -> nav produits footer -->
        <span class="footer-separation no-visible-sl"></span>
        <!-- debut -> contact footer -->
        <div id="contact"  class="col-lg-5 col-md-5">
            <h1>Nous contacter</h1>
            <div id="adresse">
                <h1>C DU BELGE</h1>
                <p>Rue de la Révision 105</p>
                <p>4032 CHENEE (Belgique)</p>
                <p>Tél: +32 (0)479/615 911</p>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2532.040596427375!2d5.62160861573713!3d50.607783179497424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c0f721b3ff171f%3A0x8049a9dbc7e8d87d!2sRue+de+la+R%C3%A9vision+105%2C+4032+Li%C3%A8ge!5e0!3m2!1sfr!2sbe!4v1513278952786" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!-- fin -> contact footer -->
    </div>
</section>
<!-- fin -> info -->

<!-- debut -> social-media -->
<section id="social-media">
    <div id="icon" class="container">
        <a href="https://www.facebook.com/C-du-belge-322090124504697/">
            <img src="asset/img/icon/faceboock.png" alt="faceboock">
        </a>
        <a href="https://twitter.com/cdubelge">
            <img src="asset/img/icon/twitter.png" alt="twitter">
        </a>
        <a href="">
            <img src="asset/img/icon/linkedin.png" alt="linkedin">
        </a>
    </div>
</section>

<!-- fin -> social-media -->
</footer>

<!-- fin -> footer -->


<!-- debut -> menu-mobile -->
<div id="mimin-mobile" class="reverse">
    <div class="mimin-mobile-menu-list">
        <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
            <ul class="nav nav-list">
                <li class="active ripple">
                    <a class="tree-toggle nav-header" href="#">
                        <span class="fa-home fa"></span>Accueil
                    </a>
                </li>
                <li class="ripple">
                    <a class="tree-toggle nav-header" href="#">
                        <span class="fa fa-list"></span>Nos produits
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                    </a>
                    <ul class="nav nav-list tree">
                        <li><a href="#">Délice en sachet</a></li>
                        <li><a href="#">Délice XL</a></li>
                        <li><a href="#">Bonbons Gicopa</a></li>
                        <li><a href="#">Délice de saison</a></li>
                    </ul>
                </li>
                <li class="ripple">
                    <a class="tree-toggle nav-header" href="#">
                        <span class="fa fa-envelope-o"></span>Nous contacter
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
    <span class="fa fa-bars"></span>
</button>
<!-- fin -> Menu-mobile -->


<!-- debut -> JAVASCRIPT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="asset/js/jquery.min.js" type="text/javascript"></script>
<script src="asset/js/jquery.ui.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>

<!-- Then Material JavaScript on top of Bootstrap JavaScript -->
<script src="asset/js/material.min.js"></script>

<!-- custom -->
<script src="asset/js/main.js"></script>
<script src="asset/js/custom.js"></script>
<script src="asset/js/favoris.js"></script>
<script src="asset/js/connetion.js"></script>
<script src="asset/js/ajout-panier.js"></script>
<script src="asset/js/form_address.js"></script>
<script src="asset/js/quantite.js"></script>



<script type="text/javascript">

</script>

<!-- fin -> JAVASCRIPT -->
</body>
</html>
