<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : dashboard.php
// c'est le dashboard du client
//======================================================================


 ?>
<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mon compte</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Bonjour <strong>Enza</strong> et bienvenue sur votre page d'accueil. <br />
            Vous pouvez y gérer vos informations personnelles ainsi que vos commandes.
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> recap -->
                <div id="recap" class="col-lg-9 col-md-12 col-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="donneePerso.php">
                                <h1>Mes données personnelle</h1>
                                <p>Changez le mot de passes</p>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="historique.php">
                                <h1>Mon historiques des commandes</h1>
                                <p>Commandes : <span>3</span></p>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="avoir.php">
                                <h1>Mes avoirs</h1>
                                <p>Avoir : <span>0</span></p>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="retourMarchandise.php">
                                <h1>Mes retours de marchandise</h1>
                                <p>Retours de marchandise : <span>0</span></p>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="bonReduc.php">
                                <h1>Mes bons de réduction</h1>
                                <p>Bons de réduction : <span>0</span></p>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="listEnvies.php">
                                <h1>Mes listes d'envies</h1>
                                <p>Listes : <span>1</span></p>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="adresses.php">
                                <h1>Mes adresses</h1>
                                <p>Mettre à jour mes adresses</p>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <a href="newsletter.php">
                                <h1>Newsletter</h1>
                                <p>Abonnement  / Désabonnement</p>
                            </a>
                        </div>
                        <!-- <div class="col-lg-4 col-md-4 col-12"></div> -->
                    </div>
                    <div class="air">
                        <a class="btn btn-success" href="index.php" role="button">
                            Déconnexion
                        </a>
                    </div>
                </div>
                <!-- fin -> recap -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
