<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : historiques.php
// ceci est l'hitoriques des commandes du client
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mon historiques des commandes</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Vous trouverez ici vos commandes passées depuis la création de votre compte
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> historiques -->
                <div id="historique" class="col-lg-9 col-md-12 col-12">
                    <!-- debut -> 1er historique -->
                    <div class="list-history">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Commande N°</h1>
                                <p>1001</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Total de la commande</h1>
                                <p>3,90 €</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Statut</h1>
                                <p>En cours</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="row btn-histoir">
                                    <div class="col-6">
                                        <a class="btn btn-success" role="button" href="detailHistorique.php">
                                            Afficher
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a class="btn btn-success" role="button" href="#">
                                            Imprimer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row air-top">
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Produits</h1>
                                <p>1</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Commade passée le</h1>
                                <p>10/10/2017</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12"></div>
                            <div class="col-lg-3 col-md-3 col-12 btn-ajout">
                                <a class="btn btn-success" role="button" href="#">
                                    Ajouter à ma commande
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- fin -> 1er historique -->

                    <!-- debut -> 2e historique -->
                    <div class="list-history">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Commande N°</h1>
                                <p>903</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Total de la commande</h1>
                                <p>9,80 €</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Statut</h1>
                                <p>Expédié</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="row btn-histoir">
                                    <div class="col-6">
                                        <a class="btn btn-success" role="button" href="#">
                                            Afficher
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a class="btn btn-success" role="button" href="#">
                                            Imprimer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row air-top">
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Produits</h1>
                                <p>2</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Commade passée le</h1>
                                <p>10/10/2017</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12"></div>
                            <div class="col-lg-3 col-md-3 col-12 btn-ajout">
                                <a class="btn btn-success" role="button" href="#">
                                    Ajouter à ma commande
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- fin -> 2e historique -->

                    <!-- debut -> 3e historique -->
                    <div class="list-history">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Commande N°</h1>
                                <p>883</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Total de la commande</h1>
                                <p>3,90 €</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Statut</h1>
                                <p>Expédié</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="row btn-histoir">
                                    <div class="col-6">
                                        <a class="btn btn-success" role="button" href="#">
                                            Afficher
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a class="btn btn-success" role="button" href="#">
                                            Imprimer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row air-top">
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Produits</h1>
                                <p>1</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <h1>Commade passée le</h1>
                                <p>13/09/2017</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12"></div>
                            <div class="col-lg-3 col-md-3 col-12 btn-ajout">
                                <a class="btn btn-success" role="button" href="#">
                                    Ajouter à ma commande
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- fin -> 3e historique -->

                    <!-- debut -> pagination  -->
                    <div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- fin -> pagination -->

                    <!-- fin -> historiques -->
                </div>
            </div>
        </section>
        <!-- fin -> dashboard -->

    </main>



    <?php include 'footer.php'; ?>
