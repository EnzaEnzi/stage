<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : panier.php
// ceci est la page d'un produit
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Cuberdon original</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debut ->info-produit -->
    <section id="info-produit" class="container">
        <div class="row">
            <!-- debut -> img-produit -->
            <div class="col-lg-5 col-md-5">
                <div>
                    <img src="asset/img/cuberdon_original.jpg" alt="Cuberdon original">
                </div>
                <div>
                    <ul class="img-list">
                        <li class="img-item active">
                            <img src="asset/img/cuberdon_original.jpg" alt="Cuberdon original">
                        </li>
                        <li class="img-item">
                            <img src="asset/img/sachet-cuberdon-original.jpg" alt="Sachet cuberdon original 10 pces">
                        </li>
                    </ul>
                </div>
            </div>
            <!-- fin -> img-produit -->
            <!-- debut -> bouton -->
            <div class="col-lg-3 col-md-3 bouton">
                <div>
                    <a href="#" class="btn btn-warning">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i> Envoyer à un ami
                    </a>
                </div>
                <div>
                    <a href="#" class="btn btn-warning">
                        <span class="icons icon-printer"></span> Imprimer
                    </a>
                </div>
                <div class="btn-media">
                    <ul>
                        <li class="media-item">
                            <a href="#">
                                <img src="asset/img/icon/crl_facebook.png" alt="Facebook">
                            </a>
                        </li>
                        <li class="media-item">
                            <a href="#">
                                <img src="asset/img/icon/crl_twitter.png" alt="Twitter">
                            </a>
                        </li>
                        <li class="media-item">
                            <a href="#">
                                <img src="asset/img/icon/crl_linkedin.png" alt="Linked In">
                            </a>
                        </li>
                        <li class="media-item">
                            <a href="#">
                                <img src="asset/img/icon/crl_google-plus.png" alt="Google +">
                            </a>
                        </li>
                        <li class="media-item">
                            <a href="#">
                                <img src="asset/img/icon/crl_pinterest.png" alt="Pinterest">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- fin -> bouton -->
            <!-- debut -> card-produit -->
            <div class="col-lg-4 col-md-4">
                <div id="card-produit" class="card">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <p class="prix">3.90 €</p><span> TTC</span>
                            <p class="prix-info">1,99 € par sachet</p>
                        </li>
                        <li class="list-group-item">
                            <div class="quantity_selector">
                                <button class="minus-btn" type="button" name="button">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                                <input id="quantity_value" type="text" name="name" value="1">
                                <button class="plus-btn" type="button" name="button">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div>
                                <button type="button" class="btn btn-outline-warning">Avoir</button>
                            </div>
                            <div class="btn-ajout">
                                <a class="btn btn-success" href="#" role="button">
                                    <img src="asset/img/icon/shopping-bag.png" alt="">
                                    Ajouter au panier
                                </a>
                            </div>
                        </li>
                        <li class="list-group-item favori-text">
                            <span class="favorite"></span>Ajouter à ma liste d’envies
                        </li>
                        <li class="list-group-item box-help">
                            <h1>UNE QUESTION ?   |   UN CONSEIL ?</h1>
                            <p>
                                Envoyer un mail ou appeler le <br />
                                0479/615 911
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- fin -> card-produit -->
        </div>
        <!-- debut -> card-file -->
        <div class="row">
            <div id="card-file" class="col-lg-7 col-md-9">
                <div class="card">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-expanded="true">
                                <img src="asset/img/icon/info-sign.png" alt="information">
                                information
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fileTech-tab" data-toggle="tab" href="#fileTech" role="tab" aria-controls="fileTech">
                                <img src="asset/img/icon/fiche_technique.png" alt="fiche technique">
                                Fiche technique
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="information" role="tabpanel" aria-labelledby="information-tab">
                            <ul class="info-list">
                                <li class="info-item">
                                    <p class="title">
                                        Cuberdon original ? artisanal - le vrai -
                                        traditionnel framboise - bonbon belge - à
                                        la gomme arabique recette de 1954 - 10 pces
                                    </p>
                                </li>
                                <li class="info-item">
                                    <p>Référence : <span>2000</span></p>
                                </li>
                                <li class="info-item">
                                    <p>État :  <span>Nouveau produit</span></p>
                                </li>
                                <li class="info-item product-description">
                                    <p>
                                        Cette spécialité belge dont la recette originale
                                        date de 1954 est un savoureux mélange de gomme
                                        arabique, de sucre et de framboises au cœur coulant
                                        forme un ruban.
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="fileTech" role="tabpanel" aria-labelledby="fileTech-tab">
                            <table class="table">
                                <tr>
                                    <td class="td-title">ingrédients</td>
                                    <td class="td-descrip">SUCRE, GOMME ARABIQUE, GLUCOSE, EAU, GELATINE, AROME FRAMBOISE</td>
                                </tr>
                                <tr>
                                    <td class="td-title">colorants</td>
                                    <td class="td-descrip">E120, E153</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin -> card-file -->
    </section>
    <!-- fin -> info-produit -->

    <!-- debut -> slider-product -->
    <section id="slider-produit" class="container no-visible-sl">
        <div class="row">
            <!-- debut -> 1er produit -->
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <a class="d-block" href="produit.php">
                        <img src="asset/img/cuberdon_original.jpg" alt="Cuberdon original">
                    </a>
                    <div class="favorite"></div>
                    <div class="box-product product-new">
                        new
                    </div>
                    <div class="card-body">
                        <h1 class="card-title">Cuberdon original</h1>
                        <p class="prix">3,90 €</p>
                    </div>
                    <div class="hovered">
                        <p>
                            artisanal - le vrai - traditionnel framboise -
                            bonbon belge - à la gomme arabique recette
                            de 1954 - 10 pces
                        </p>
                        <div class="row">
                            <div class="col-6">
                                <a class="btn btn-light" href="produit.php" role="button">
                                    Détail
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-success btn-addcart" href="#" role="button">
                                    Ajouter au panier
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin -> 1er produit -->

            <!-- debut -> 2e produit -->
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <a class="d-block" href="#">
                        <img class="img-produit EMPORARYIMGFIX" src="asset/img/melocake_lait.jpg" alt="Mélo cakes tradition lait">
                    </a>
                    <div class="favorite"></div>
                    <div class="card-body">
                        <h1 class="card-title">Mélo cakes tradition lait</h1>
                        <p class="prix">3,49 €</p>
                    </div>
                    <div class="hovered">
                        <p>
                            ecette artisanale - un vrai biscuit sablé -
                            du bon chocolat - maison belge - sachet 3 pièces
                        </p>
                        <div class="row">
                            <div class="col-6">
                                <a class="btn btn-light" href="#" role="button">
                                    Détail
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-success btn-addcart" href="#" role="button">
                                    Ajouter au panier
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin -> 2e produit -->

            <!-- debut -> 3e produit -->
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <a class="d-block" href="#">
                        <img class="img-produit EMPORARYIMGFIX" src="asset/img/viollet_accidule.jpg" alt="Plaque sure viollet">
                    </a>
                    <div class="favorite"></div>
                    <div class="card-body">
                        <h1 class="card-title">Plaque sure viollet</h1>
                        <p class="prix">3,49 €</p>
                    </div>
                    <div class="hovered">
                        <p>
                            Plaque sure - sûre - bonbon acidulé violette
                            - placard - langue de chat - 8 pces
                        </p>
                        <div class="row">
                            <div class="col-6">
                                <a class="btn btn-light" href="#" role="button">
                                    Détail
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-success btn-addcart" href="#" role="button">
                                    Ajouter au panier
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin -> 3e produit -->

            <!-- debut -> 4e produit -->
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <a class="d-block" href="#">
                        <img class="img-produit" src="asset/img/lard_choco_fondant.jpg" alt="Lard chocolat fondant nor et café">
                    </a>
                    <div class="favorite"></div>
                    <div class="box-product product-promo">
                        Promo
                    </div>
                    <div class="card-body">
                        <h1 class="card-title">Lard chocolat noir et café</h1>
                        <p class="prix">2,99 €</p>
                    </div>
                    <div class="hovered">
                        <p>
                            LARD - GUIMAUVE CHOCOLAT FONDANT NOIR et café -
                            recette artisan belge - LE CAFE - 6 pces
                        </p>
                        <div class="row">
                            <div class="col-6">
                                <a class="btn btn-light" href="#" role="button">
                                    Détail
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-success btn-addcart" href="#" role="button">
                                    Ajouter au panier
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin -> 4e produit -->
        </div>

    </section>
    <!-- fin -> slider-product -->
</main>

<?php include 'footer.php'; ?>
