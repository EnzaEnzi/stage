<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : newsletter.php
// ceci est le formulaire qui permet au client de s'inscrire ou pas à notre newsletter
//======================================================================


 ?>
<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Newsletter</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <!-- <div id="message" class="container">
        <p>

        </p>
    </div> -->
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> form_newsletter -->
                <div id="form_newsletter" class="col-lg-9 col-md-12 col-12">
                    <p>vous pouvez désabonner votre abonnement gratuitement à tout moment.</p>
                    <div id="box-newsletter">
                        <form action="\" name="newsletter" method="post">
                            <div>
                                <label for="newsletterSelect">Je souhaite recevoir la newsletter:</label>
                                <select name="status" id="newsletterSelect">
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <div class="clear"></div>
                            </div>
                            <div>
                                <input class="btn btn-success" name="save" value="Enregistrer" type="submit">
                                <div class="clear"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- fin -> form_newsletter -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
