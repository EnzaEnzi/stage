<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : connetion.php
// Page de connexion
//======================================================================



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login | register</title>

    <!-- debut -> css -->

    <!-- Add Material font (Roboto) and Material icon as needed -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- Add Material CSS, replace Bootstrap CSS -->
    <link href="asset/css/material.css" rel="stylesheet" />

    <!-- add plugin CSS -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/flags.min.css" />
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>


    <!-- add custom CSS -->
    <link rel="stylesheet" type="text/css" href="asset/css/style.css">
    <link rel="stylesheet" type="text/css" href="asset/css/connetion.css">
    <!-- fin -> css -->

</head>
<body id="bg-connetion">
    <div id="box-connetion" class="container no-visible-sl">
        <div class="backbox">
            <div class="loginMsg">
                <div class="textcontent">
                    <p class="title">Don’t Have an account?</p>
                    <p>Sign up to save all your graph.</p>
                    <button id="switch1">Sign Up</button>
                </div>
            </div>
            <div class="signupMsg visibility">
                <div class="textcontent">
                    <p class="title">Have an account?</p>
                    <p>Log in to see all your collection.</p>
                    <button id="switch2">LOG IN</button>
                </div>
            </div>
        </div>
        <!-- backbox -->

        <div class="frontbox">
            <div class="login">
                <h2>LOG IN</h2>
                <div class="inputbox">
                    <div class="">
                        <input type="text" name="email" placeholder="  EMAIL">
                        <span class="icons icon-envelope"></span>
                    </div>
                    <div class="">
                        <input type="password" name="password" placeholder="  PASSWORD">
                        <span class="icons icon-lock"></span>
                    </div>

                </div>
                <p>FORGET PASSWORD?</p>
                <a href="dashboard.php">
                    <button>
                        LOG IN
                    </button>
                </a>
            </div>

            <div class="signup hide">
                <h2>SIGN UP</h2>
                <div class="inputbox">
                    <div>
                        <input type="text" name="fullname" placeholder="  FULLNAME">
                        <span class="icons icon-user"></span>
                    </div>
                    <div>
                        <input type="text" name="email" placeholder="  EMAIL">
                        <span class="icons icon-envelope"></span>
                    </div>
                    <div>
                        <input type="password" name="password" placeholder="  PASSWORD">
                        <span class="icons icon-lock"></span>
                    </div>
                </div>
                <button>
                    SIGN UP
                </button>
            </div>

        </div>
        <!-- frontbox -->
    </div>

    <!-- debut -> connetion-mobile -->
    <div id="box-mobile" class="connetion-mobile no-visible">
        <ul class="tab-group">
            <li class="tab"><a href="#singup-mobile">Sign Up</a></li>
            <li class="tab active"><a href="#login-mobile">Log In</a></li>
        </ul>

        <div class="tab-content">
            <!-- debut -> login-mobile -->
            <div id="login-mobile">
                <form action="/" method="post">
                    <div class="field-wrap">
                        <label>
                            <span class="icons icon-envelope"></span>
                            Email<span class="req">*</span>
                        </label>
                        <input type="email"required autocomplete="off"/>
                    </div>
                    <div class="field-wrap">
                        <label>
                            <span class="icons icon-lock"></span>
                            Password<span class="req">*</span>
                        </label>
                        <input type="password"required autocomplete="off"/>
                    </div>
                    <p class="forgot"><a href="#">Forgot Password?</a></p>
                    <button class="button button-block"/>Log In</button>
                </form>
            </div>
            <!-- fin -> login-mobile -->

            <!-- debut -> singup-mobile -->
            <div id="singup-mobile">
                <form action="/" method="post">
                    <div class="field-wrap">
                        <label>
                            <span class="icons icon-user"></span>
                            Full Name<span class="req">*</span>
                        </label>
                        <input type="text" required autocomplete="off" />
                    </div>
                    <div class="field-wrap">
                        <label>
                            <span class="icons icon-envelope"></span>
                            Email<span class="req">*</span>
                        </label>
                        <input type="email"required autocomplete="off"/>
                    </div>
                    <div class="field-wrap">
                        <label>
                            <span class="icons icon-lock"></span>
                            Password<span class="req">*</span>
                        </label>
                        <input type="password"required autocomplete="off"/>
                    </div>
                    <button type="submit" class="button button-block"/>Sign Up</button>
                </form>
            </div>
            <!-- fin -> singup-mobile -->


        </div><!-- tab-content -->
    </div> <!-- /form -->

    <!-- fin -> connetion-> mobile -->
</body>
<footer>
    <!-- debut -> JAVASCRIPT -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="asset/js/jquery.min.js" type="text/javascript"></script>
    <script src="asset/js/jquery.ui.min.js"></script>
    <script src="asset/js/bootstrap.min.js"></script>

    <!-- Then Material JavaScript on top of Bootstrap JavaScript -->
    <script src="asset/js/material.min.js"></script>

    <!-- custom -->
    <script src="asset/js/main.js"></script>
    <script src="asset/js/custom.js"></script>
    <script src="asset/js/favoris.js"></script>
    <script src="asset/js/connetion.js"></script>

    <!-- fin -> JAVASCRIPT -->
</footer>

</html>
