<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : detailHistoriques.php
// ceci est est le détail d'un "list-history" de commande du client
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Information commandes</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Détail commande ma N° 1001 passée le 10/10/2017
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> info_commande -->
                <div id="info_commande" class="col-lg-9 col-md-12 col-12">
                    <!-- debut -> header-facturation -->
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="card">
                                <h1 class="card-title bg-warning">Adresse de livraison</h1>
                                <p class="card-text">
                                    Enza Lombardo <br />
                                    Rue Pierreuse 79/11 <br />
                                    4000 Liège <br />
                                    Belgique
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="card">
                                <h1 class="card-title bg-warning">Adresse de facturation</h1>
                                <p class="card-text">
                                    Enza Lombardo <br />
                                    Rue Pierreuse 79/11 <br />
                                    4000 Liège <br />
                                    Belgique
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="card">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <h1>Statut</h1>
                                        <span>En cours</span>
                                    </li>
                                    <li class="list-group-item">
                                        <h1>Mode de paiment</h1>
                                        <span><i class="fa fa-cc-mastercard"></i></span>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <!-- fin ->header-facturation  -->

                    <!-- debut -> main-facturation -->
                    <div class="row air">
                        <div class="col-lg-9 col-md-9">

                            <!-- debut -> tableau -->
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Qté.</th>
                                        <th scope="col">Produit(s)</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="quantity">1</td>
                                        <td class="product">
                                            <img src="asset/img/cuberdon_original.jpg" alt="Cuberdon original" class="no-visible-sl">
                                            <a href="produit.php">Cuberdon original</a>
                                            <p> Référence produit 2000 </p>
                                        </td>
                                        <td class="price">3,90 €</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr class="sous-total">
                                        <td colspan="2">Sous-total Produit TTC</td>
                                        <td>3,90 €</td>
                                    </tr>
                                    <tr class="taxe">
                                        <td colspan="2">Taxes</td>
                                        <td>0,22 €</td>
                                    </tr>
                                    <tr class="total">
                                        <td colspan="2">Total</td>
                                        <td>3,90 €</td>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- fin -> tableau -->

                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="btn-detailStory">
                                <a href="#" class="btn btn-success">
                                    Imprimer
                                </a>
                            </div>
                            <div class="btn-detailStory">
                                <a href="#" class="btn btn-success">
                                    Annuler
                                </a>
                            </div>
                            <div class="btn-detailStory">
                                <a href="#" class="btn btn-success">
                                    Retourner
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- fin -> main-facturation -->

                    <!-- fin -> info_commande -->
                </div>

            </div>
        </section>
        <!-- fin -> dashboard -->

    </main>



    <?php include 'footer.php'; ?>
