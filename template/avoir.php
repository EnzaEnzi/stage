<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : avoir.php
// ceci est la liste des avoirs du client
//======================================================================

 ?>
 <?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mes avoirs</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Liste des avoirs reçus suite à des annulations de commandes.
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> avoir -->
                <div id="avoir" class="col-lg-9 col-md-12 col-12">
                    <p>Vous n'avez reçu aucun avoir</p>
                </div>
                <!-- fin -> avoir -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
