<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : adresses.php
// ceci est la liste d'adresse du client
//======================================================================

?>
<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mes adresses</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Choisissez vos adresses de facturation et de livraison. Ces dernières
            seront présélectionnées lors de vos commandes. Vous pouvez également
            ajouter d'autres adresses, ce qui est particulièrement intéressant
            pour envoyer des cadeaux ou recevoir votre commande au bureau.
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> mes_adresse -->
                <div id="mes_adresse" class="col-lg-9 col-md-12 col-12">
                    <!-- debut -> item-address -->
                    <div class="item-address">
                        <div class="row">
                            <div class="col-lg-8 col-md-8">
                                <h1>Enza LOMBARDO</h1>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <a href="#" class="btn btn-success btn-facturation">
                                    Adresse de facturation
                                </a>
                            </div>
                        </div>
                        <div class="row air-top">
                            <div class="col-lg-8 col-md-8 adresse">
                                <p>Rue Pierreuse 79/11</p>
                                <p>4000 LIEGE</p>
                                <p>Belgique</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-md-4">
                                <div class="row btn-address">
                                    <div class="col-6">
                                        <a href="#" class="btn btn-success toggle-panel-edit-address">
                                            Editer
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a href="" class="btn btn-success">
                                            supprimer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fin -> item-address -->

                    <!-- debut -> form_address-livraison -->
                    <div class="card panel-edit-address" style="display:none">
                        <form>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="sexe" id="sexe-f" value="f" checked>
                                    Madame
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label left">
                                    <input class="form-check-input" type="radio" name="sexe" id="sexe-h" value="h">
                                    Monsieur
                                </label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control case" id="prenom" placeholder="Votre prénom">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control case" id="nom" placeholder="Votre nom">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control case" id="adresse" placeholder="Votre adresse">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control case" id="cp" placeholder="Code postal">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control case" id="ville" placeholder="Ville">
                            </div>
                            <select class="form-control case-select">
                                <option>Belgique</option>
                                <option>France</option>
                            </select>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    <span>Sélectionner comme adresse principale</span>
                                </label>
                            </div>
                            <div class="">
                                <input class="btn btn-success" name="save" value="Enregistrer" type="submit">
                            </div>
                        </form>
                    </div>
                    <!-- fin -> form_address -->

                </div>
                <!-- fin -> mes_adresse -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
