<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : listProduit.php
// ceci est liste des produits
//======================================================================
?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Découvrez nos produits</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Confiseries artisanales et spécialités belges, produit de terroir
            comme le vrai cuberdon original, les plaques sûres, bonbons cassés,
            lards rose et blanc, chocolat noir, lards framboise, caramel, café,
            coco, chocolat lait , mendiants champion du monde de pâtisserie
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> listProduit -->
    <section>

        <div id="message-ajout" class="showMessage"></div>
        <div class="container">
            <div class="row">
                <!-- debut -> nav-second -->
                <div class="col-lg-l3 no-visible-md">
                    <nav class="nav-second">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">
                                    <span class="icons icon-arrow-right"></span>
                                    Délice en sachet
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <span class="icons icon-arrow-right"></span>
                                    Délice XL
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <span class="icons icon-arrow-right"></span>
                                    Bonbons Gicopa
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <span class="icons icon-arrow-right"></span>
                                    Délice de saison
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- fin -> nav-second -->

                <!-- debut -> nav-md -->
                <div class="col-md-12 no-visible visible nav-md">
                    <div class="dropdown">
                        <button aria-expanded="false" aria-haspopup="true" class="btn dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton" type="button">
                            Nos produits
                        </button>
                        <div aria-labelledby="dropdownMenuButton" class="dropdown-menu dropdown-menu-sm">
                            <a class="dropdown-item" href="#">Délice en sachet</a>
                            <a class="dropdown-item" href="#">Délice XL</a>
                            <a class="dropdown-item" href="#">Bonbons Gicopa</a>
                            <a class="dropdown-item" href="#">Délice de saison</a>
                        </div>
                    </div>
                </div>
                <!-- debut -> nav-md -->


                <!-- debut -> gallery -->
                <div id="gallery" class="col-lg-9 col-md-10">
                    <div class="row">
                        <!-- debut -> 1er produit -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <a class="d-block" href="produit.php">
                                    <img src="asset/img/cuberdon_original.jpg" alt="Cuberdon original">
                                </a>
                                <div class="favorite"></div>
                                <div class="box-product product-new">
                                    new
                                </div>
                                <div class="card-body">
                                    <h1 class="card-title">Cuberdon original</h1>
                                    <p class="prix">3,90 €</p>
                                </div>
                                <div class="hovered">
                                    <p>
                                        artisanal - le vrai - traditionnel framboise -
                                        bonbon belge - à la gomme arabique recette
                                        de 1954 - 10 pces
                                    </p>
                                    <div class="row">
                                        <div class="col-6">
                                            <a class="btn btn-light" href="produit.php" role="button">
                                                Détail
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a class="btn btn-success btn-addcart" href="#" role="button">
                                                Ajouter au panier
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fin -> 1er produit -->

                        <!-- debut -> 2e produit -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <a class="d-block" href="#">
                                    <img class="img-produit EMPORARYIMGFIX" src="asset/img/melocake_lait.jpg" alt="Mélo cakes tradition lait">
                                </a>
                                <div class="favorite"></div>
                                <div class="card-body">
                                    <h1 class="card-title">Mélo cakes tradition lait</h1>
                                    <p class="prix">3,49 €</p>
                                </div>
                                <div class="hovered">
                                    <p>
                                        ecette artisanale - un vrai biscuit sablé -
                                        du bon chocolat - maison belge - sachet 3 pièces
                                    </p>
                                    <div class="row">
                                        <div class="col-6">
                                            <a class="btn btn-light" href="#" role="button">
                                                Détail
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a class="btn btn-success btn-addcart" href="#" role="button">
                                                Ajouter au panier
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fin -> 2e produit -->
                    </div>

                    <div class="row">
                        <!-- debut -> 3e produit -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <a class="d-block" href="#">
                                    <img class="img-produit EMPORARYIMGFIX" src="asset/img/viollet_accidule.jpg" alt="Plaque sure viollet">
                                </a>
                                <div class="favorite"></div>
                                <div class="card-body">
                                    <h1 class="card-title">Plaque sure viollet</h1>
                                    <p class="prix">3,49 €</p>
                                </div>
                                <div class="hovered">
                                    <p>
                                        Plaque sure - sûre - bonbon acidulé violette
                                        - placard - langue de chat - 8 pces
                                    </p>
                                    <div class="row">
                                        <div class="col-6">
                                            <a class="btn btn-light" href="#" role="button">
                                                Détail
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a class="btn btn-success btn-addcart" href="#" role="button">
                                                Ajouter au panier
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fin -> 3e produit -->

                        <!-- debut -> 4e produit -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <a class="d-block" href="#">
                                    <img class="img-produit" src="asset/img/lard_choco_fondant.jpg" alt="Lard chocolat fondant nor et café">
                                </a>
                                <div class="favorite"></div>
                                <div class="box-product product-promo">
                                    Promo
                                </div>
                                <div class="card-body">
                                    <h1 class="card-title">Lard chocolat noir et café</h1>
                                    <p class="prix">2,99 €</p>
                                </div>
                                <div class="hovered">
                                    <p>
                                        LARD - GUIMAUVE CHOCOLAT FONDANT NOIR et café -
                                        recette artisan belge - LE CAFE - 6 pces
                                    </p>
                                    <div class="row">
                                        <div class="col-6">
                                            <a class="btn btn-light" href="#" role="button">
                                                Détail
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a class="btn btn-success btn-addcart" href="#" role="button">
                                                Ajouter au panier
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fin -> 4e produit -->
                    </div>

                    <!-- debut -> pagination  -->
                    <div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- fin -> pagination -->
                </div>
                <!-- fin -> gallery -->
            </div>
        </div>
    </section>
    <!-- fin -> listProduit -->

</main>



<?php include 'footer.php'; ?>
