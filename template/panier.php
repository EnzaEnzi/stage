<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : panier.php
// ceci est la page panier d'achat
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mon Panier</h1>
        </div>
    </section>

    <!-- debut -> stepper -->
    <section id="step" class="container no-visible-sl">
        <div class="stepper-horiz">
            <div class="stepper done active">
                <div class="stepper-icon ">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <!-- <div class="stepper">
                <div class="stepper-icon">
                    <span>2</span>
                </div>
                <span class="stepper-text">Connexion</span>
            </div> -->
            <div class="stepper">
                <div class="stepper-icon">
                    <span>2</span>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>3</span>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>4</span>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper -->

    <!-- debut -> stepper-mobile -->
    <section id="stepper-mobile" class="container no-visible">
        <div class="stepper-vert">
            <div class="stepper done active">
                <div class="stepper-icon ">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <!-- <div class="stepper">
                <div class="stepper-icon">
                    <span>2</span>
                </div>
                <span class="stepper-text">Connexion</span>
            </div> -->
            <div class="stepper">
                <div class="stepper-icon">
                    <span>2</span>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>3</span>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>4</span>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper-mobile -->

    <!-- debut -> box-shopping -->
    <section id="box-shopping" class="container">
        <div class="row">
            <!-- debut -> table-commande -->
            <div class="col-lg-8 col-md-8">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Produit(s)</th>
                            <th scope="col" class="no-visible-sl"></th>
                            <th scope="col">Qté.</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- debut -> item produit -->
                        <tr>
                            <td class="product">
                                <img src="asset/img/cuberdon_original.jpg" alt="Cuberdon original" class="no-visible-sl">
                                <a href="produit.php">
                                    <h1>Cuberdon original</h1>
                                    <p> Référence produit 2000 </p>
                                    <span>Voir la fiche descriptive</span>
                                </a>
                            </td>
                            <td class="icon no-visible-sl">
                                <span class="favorite"></span>
                                <span class="delete"></span>
                            </td>
                            <td class="quantite">
                                <div class="quantity_selector">
                                    <button class="minus-btn" type="button" name="button">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </button>
                                    <input id="quantity_value" type="text" name="name" value="1">
                                    <button class="plus-btn" type="button" name="button">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </div>


                            </td>
                            <td class="prix">3,90 €</td>
                        </tr>
                        <!-- fin -> item produit -->
                    </tbody>
                    <tfoot>
                        <tr>
                            <td id="box-bonReduc" colspan="4">
                                <form action="/" name="save-bonReduc" method="post">
                                    <div>
                                        <label for="codeReduc">Bons de réduction</label>
                                        <input id="codeReduc" class="case" name="codeReduc" type="text">
                                    </div>
                                    <div>
                                        <input class="btn btn-success" name="save" value="ok" type="submit">
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- fin -> table-commande -->

            <!-- debut -> card-commande -->
            <div class="col-lg-4 col-md-4">
                <div id="card-commande" class="card sticky">
                    <div class="card-body">
                        <h4 class="card-title">Ma commande</h4>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td colspan="2">Sous-total TTC</td>
                                    <td>3,90 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Taxes</td>
                                    <td>0,22 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Sous-total TTC</td>
                                    <td>3,90 €</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div>
                                <a class="btn btn-success" href="panier-step2.php" role="button">
                                    Valider mon panier
                                </a>
                            </div>
                        </li>
                        <li class="list-group-item box-help">
                            <h1>Besoin d'aide ?</h1>
                            <p>
                                Envoyer un mail ou appeler le <br />
                                0479/615 911
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- fin -> card-commande -->
        </div>

        <!-- debut -> btn-shopping -->
        <div class="btn-shopping">
            <div class="row">
                <div class="col-6">
                    <a href="listProduit.php" class="btn btn-dark btn-retour">
                        Continuer mes achats
                    </a>
                </div>
                <div class="col-6">
                    <a href="panier-step2.php" class="btn btn-success btn-continuer">
                        Valider mon panier
                    </a>
                </div>
            </div>
        </div>
        <!-- fin -> btn-shopping -->

        <div class="panier_estimation">
            <h1>Estimation des frais de livraison</h1>
            <p>
                Merci de vous <a href="connexion.php">identifier</a>, pour afficher
                les frais de livraison correcpondant à vos données personnelles
            </p>
        </div>
    </section>
    <!-- fin -> box-shopping -->



</main>

<?php include 'footer.php'; ?>
