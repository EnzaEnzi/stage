<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : avoir.php
// ceci est la liste des bon de réduction du client
//======================================================================


 ?>
<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mes bons de réduction</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <!-- <div id="message" class="container">
        <p>

        </p>
    </div> -->
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> bon_reduc -->
                <div id="bon_reduc" class="col-lg-9 col-md-12 col-12">
                    <p>Vous ne possédez pas de bon de réduction.</p>
                </div>
                <!-- fin -> bon_reduc -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
