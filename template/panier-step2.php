<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : panier-step2.php
// ceci est la page panier d'achat à la 2e étape -> mode de livraison
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mon Panier</h1>
        </div>
    </section>

    <!-- debut -> stepper -->
    <section id="step" class="container no-visible-sl">
        <div class="stepper-horiz">
            <div class="stepper">
                <div class="stepper-icon ">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <div class="stepper done active">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>3</span>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>4</span>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper -->

    <!-- debut -> stepper-mobile -->
    <section id="stepper-mobile" class="container no-visible">
        <div class="stepper-vert">
            <div class="stepper">
                <div class="stepper-icon ">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <div class="stepper done active">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>3</span>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <span>4</span>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper-mobile -->

    <!-- debut -> box-address -->
    <section id="box-address" class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <!-- debut -> mode-livraison -->
                <div id="mode-livraison">
                    <h1>Veuillez sélectionner le mode de livraison désiré pour l'envoi de cette commande.</h1>
                    <form action="/" name="mode-livraison" method="post">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="2">Transporteur</th>
                                    <th>Information</th>
                                    <th>Prix</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- debut -> item-mode-livraison -->
                                <tr>
                                    <td class="check-radio">
                                        <input id="1" name="livraison_id" value="" checked="checked" type="radio">
                                    </td>
                                    <td class="transport no-visible-sl">
                                        <img src="asset/img/Bpost_2010_(logo).svg.png" alt="">
                                    </td>
                                    <td>
                                        Récupérez votre colis dans un des nombreux points d'enlèvement.
                                    </td>
                                    <td class="offert">
                                        Offert
                                    </td>
                                </tr>
                                <!-- fin -> item-mode-livraison -->

                                <!-- debut -> item-mode-livraison -->
                                <tr>
                                    <td class="check-radio">
                                        <input id="2" name="livraison_id" value="" type="radio">
                                    </td>
                                    <td class="transport no-visible-sl">
                                        <img src="asset/img/Bpost_2010_(logo).svg.png" alt="">
                                    </td>
                                    <td>
                                        Recevez votre colis à domicile ou au bureau.
                                    </td>
                                    <td class="offert">
                                        Offert
                                    </td>
                                </tr>
                                <!-- fin -> item-mode-livraison -->
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- fin -> mode-livraison -->
            </div>
            <!-- debut -> card-commande -->
            <div class="col-lg-4 col-md-4">
                <div id="card-commande" class="card sticky">
                    <div class="card-body">
                        <h4 class="card-title">Ma commande</h4>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td colspan="2">Sous-total TTC</td>
                                    <td>3,90 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Taxes</td>
                                    <td>0,22 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Sous-total TTC</td>
                                    <td>3,90 €</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div>
                                <a class="btn btn-success" href="panier-step3.php" role="button">
                                    Continuer
                                </a>
                            </div>
                        </li>
                        <li class="list-group-item box-help">
                            <h1>Besoin d'aide ?</h1>
                            <p>
                                Envoyer un mail ou appeler le <br />
                                0479/615 911
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- fin -> card-commande -->
        </div>
    </section>
    <!-- fin -> box-address-->

    <!-- debut -> box-address-livraison -->
    <section id="box-address-livraison" class="container">
        <h1>Je choisis mon adresse de livraison</h1>
        <div  class="row">
            <!-- debut -> adresse-facturation -->
            <div class="col-lg-6 col-md-6">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">Adresse de facturation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="check-radio">
                                <input name="adresse" value="adresse-facturation" checked="checked" type="radio">
                            </td>
                            <td class="methodRow">
                                <p>
                                    Enza Lombardo <br />
                                    Rue Pierreuse 79/11 <br />
                                    4000 Liège <br />
                                    Belgique
                                </p>
                                <a href="#">Modifier cette adresse</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- fin -> adresse-facturation -->

            <!-- debut -> adresse-livraison -->
            <div class="col-lg-6 col-md-6">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">Adresse de livraison</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="check-radio">
                                <input name="adresse" value="adresse-livraison"  type="radio">
                            </td>
                            <td class="methodRow">
                                <p>
                                    Enza Lombardo <br />
                                    Rue Pierreuse 79/11 <br />
                                    4000 Liège <br />
                                    Belgique
                                </p>
                                <a href="#" class="toggle-panel-edit-address">
                                    Modifier cette adresse
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- fin -> box-address-livraison -->
        </div>

        <div class="new-address">
            <a href="#" class="toggle-panel-edit-address">
                J'ajoute une autre adresse à mon carnet d'adresses
            </a>
        </div>

        <!-- debut -> btn-shopping -->
        <div class="btn-shopping">
            <div class="row">
                <div class="col-6">
                    <a href="panier.php" class="btn btn-dark btn-retour">
                        Retour à mon panier
                    </a>
                </div>
                <div class="col-6">
                    <a href="panier-step3.php" class="btn btn-success btn-continuer">
                        continuer
                    </a>
                </div>
            </div>
        </div>
        <!-- fin -> btn-shopping -->

        <!-- debut -> form_address-livraison -->
        <div class="card panel-edit-address" style="display:none">
            <form>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="sexe" id="sexe-f" value="f" checked>
                        Madame
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label left">
                        <input class="form-check-input" type="radio" name="sexe" id="sexe-h" value="h">
                        Monsieur
                    </label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control case" id="prenom" placeholder="Votre prénom">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control case" id="nom" placeholder="Votre nom">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control case" id="adresse" placeholder="Votre adresse">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control case" id="cp" placeholder="Code postal">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control case" id="ville" placeholder="Ville">
                </div>
                <select class="form-control case-select">
                    <option>Belgique</option>
                    <option>France</option>
                </select>

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input">
                        <span>Sélectionner comme adresse principale</span>
                    </label>
                </div>
                <div class="">
                    <input class="btn btn-success" name="save" value="Enregistrer" type="submit">
                </div>
            </form>
        </div>
        <!-- fin -> form_address-livraison -->
    </section>
    <!-- fin -> adresse-livraison -->


</main>

<?php include 'footer.php'; ?>
