<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : nav-user.php
// ceci est la navigation secondaire du dashboard => espace client
//======================================================================


?>

<!-- debut -> nav-second -->
<div class="col-lg-l3 no-visible-md">
    <nav class="nav-user">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="donneePerso.php">
                    <span class="icons icon-arrow-right"></span>
                    Mes données personnelles
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="historique.php">
                    <span class="icons icon-arrow-right"></span>
                    Mon historiques des commandes
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="avoir.php">
                    <span class="icons icon-arrow-right"></span>
                    Mes avoirs
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="retourMarchandise.php">
                    <span class="icons icon-arrow-right"></span>
                    Mes retours de marchandise
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="bonReduc.php">
                    <span class="icons icon-arrow-right"></span>
                    Mes bons de réduction
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="listEnvies.php">
                    <span class="icons icon-arrow-right"></span>
                    Mes listes d'envies
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="adresses.php">
                    <span class="icons icon-arrow-right"></span>
                    Mes adresses
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="newsletter.php">
                    <span class="icons icon-arrow-right"></span>
                    Newsletter
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php">
                    <span class="icons icon-arrow-right"></span>
                    Déconnexion
                </a>
            </li>
        </ul>
    </nav>
</div>
<!-- fin -> nav-second -->

<!-- debut -> nav-md -->
<div class="col-md-12 no-visible visible nav-md">
    <div class="dropdown">
        <button aria-expanded="false" aria-haspopup="true" class="btn dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton" type="button">
            Mon compte
        </button>
        <div aria-labelledby="dropdownMenuButton" class="dropdown-menu dropdown-menu-sm">
            <a class="dropdown-item" href="donneePerso.php">
                Mes données personnelles
            </a>
            <a class="dropdown-item" href="historique.php">
                Mon historiques des commandes
            </a>
            <a class="dropdown-item" href="avoir.php">
                Mes avoirs
            </a>
            <a class="dropdown-item" href="retourMarchandise.php">
                Mes retours de marchandise
            </a>
            <a class="dropdown-item" href="bonReduc.php">
                Mes bons de réduction
            </a>
            <a class="dropdown-item" href="listEnvies.php">
                Mes listes d'envies
            </a>
            <a class="dropdown-item" href="adresses.php">
                Mes adresses
            </a>
            <a class="dropdown-item" href="newsletter.php">
                Newsletter
            </a>
            <a class="dropdown-item" href="index.php">
                Déconnexion
            </a>
        </div>
    </div>
</div>
<!-- debut -> nav-md -->
