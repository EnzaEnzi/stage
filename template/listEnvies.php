<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : listEnvies.php
// ceci est la liste des envies du client
//======================================================================


 ?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mes listes d'envies</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <!-- <div id="message" class="container">
        <p>

        </p>
    </div> -->
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div id="message-ajout" class="showMessage"></div>
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> gallery-envies -->
                <div id="gallery-envies" class="col-lg-9 col-md-12 col-12">
                    <div class="row">
                        <!-- debut -> 1e produit -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <a class="d-block" href="#">
                                    <img class="img-produit EMPORARYIMGFIX" src="asset/img/melocake_lait.jpg" alt="Mélo cakes tradition lait">
                                </a>
                                <div class="favorite"></div>
                                <div class="card-body">
                                    <h1 class="card-title">Mélo cakes tradition lait</h1>
                                    <p class="prix">3,49 €</p>
                                </div>
                                <div class="hovered">
                                    <p>
                                        ecette artisanale - un vrai biscuit sablé -
                                        du bon chocolat - maison belge - sachet 3 pièces
                                    </p>
                                    <div class="row">
                                        <div class="col-6">
                                            <a class="btn btn-light" href="#" role="button">
                                                Détail
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a class="btn btn-success btn-addcart" href="#" role="button">
                                                Ajouter au panier
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fin -> 1e produit -->
                    </div>
                    <div class="air">
                        <div class="btn btn-dark bouton"  role="button">
                            <i class="fa fa-trash-o"></i>
                            Supprimer les produits
                        </div>
                    </div>
                </div>
                <!-- fin -> gallery-envies -->
            </div>

        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
