<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : contact.php
// ceci est le formulaire de contact
//======================================================================



?>
<?php include 'header.php'; ?>

<main>
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Formulaire de contact</h1>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <!-- debut -> form_contact -->
            <div class="col-lg-9 col-md-9 bg-cartePostal">
                <div id="form_contact">
                    <div id="box-contact">
                        <form action="\" name="changePassword" method="post">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 left">
                                    <div>
                                        <input id="nom" name="nom" type="text" placeholder="Votre nom">
                                        <div class="clear"></div>
                                    </div>
                                    <div>
                                        <input id="mail" name="mail" type="email" placeholder="Votre e-mail">
                                        <div class="clear"></div>
                                    </div>
                                    <div>
                                        <input id="sujet" name="sujet" type="text" placeholder="Sujet">
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 right">
                                    <div>
                                        <textarea name="your-message" id="your-message" cols="30" rows="10" placeholder="Votre message"></textarea>
                                    </div>
                                    <div>
                                        <input class="btn btn-success" name="envoyer" value="Envoyer" type="submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- fin -> form_contact -->

            <!-- debut -> nos_coordonnees -->
            <div id="nos_coordonnees" class="col-lg-3 col-md-3">
                <h1 class="titre">Nos coordonnées</h1>
                <div class="box-coordonnee">
                    <h1>C DU BELGE</h1>
                    <p>
                        Rue de la Révision 105 <br />
                        4032 CHENEE (Belgique) <br />
                        Tél : +32 (0)479/615 911
                    </p>
                </div>
            </div>
            <!-- fin -> nos_coordonnees -->
        </div>
    </section>

</main>

<?php include 'footer.php'; ?>
