/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// AJOUT - PANIER
//======================================================================

$(function() {

    // jquery : message
    // quant je clic sur le bouton "ajout de au panier" de la page listProduit
    // j'ai un message me disant que tel produit à bient été ajouté à ma commande

    function afficheMessage(texte){
        $('#message-ajout').text(texte)
                           .fadeIn(300)
                           .delay(2000)
                           .fadeOut(300);
    }

    $('.btn-addcart').each(function() {
        var nameProduct = $(this).parent().parent().parent().parent().find('.card-title').html();
        var texte = " été ajouté à votre panier"

        $(this).on('click', function() {
            afficheMessage(nameProduct + texte);
        });
    });

    // jquery : prix
    // quant je clic sur le bouton "ajout au panier" de la page listProduit
    // il ajoute le montant au panier d'achat de l'entête

    $('.btn-addcart').each(function() {
        var prix = $(this).parent().parent().parent().parent().find('.prix').html();
        var code = '<span>' + prix + '</span>'

        $(this).on('click', function(){
            $('#panier div').prepend(prix)
                            .find('#prix div')
                            .hide()
                            .slideDown();
        });
    });


    //jquety : quantité prix
    // quant je clic sur le bouton "ajouter au panier" de la page produits
    // il ajoute le montant * la quantité au panier d'achant de l'entête

    $('.btn-ajout').each(function() {
        var prix = $(this).parent().prev().find('.prix').html();
        $(this).on('click', function() {
            $('#panier div').prepend(prix)
                            .find('#prix div')
                            .hide()
                            .slideDown();
        });
    });

});
