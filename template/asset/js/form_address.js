/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// FORM_ADRESS-LIVRAISON
//======================================================================

$(function(){

    $('td.methodRow').on('click', function(e){
        e.preventDefault();
        $('.panel-edit-address').css({display: 'block'});
    });

    $('.new-address').on('click', function(e){
        e.preventDefault();
        $('.panel-edit-address').css({display: 'block'});
    });

    $('.btn-address').on('click', function(e){
        e.preventDefault();
        $('.panel-edit-address').css({display: 'block'});
    });

});
