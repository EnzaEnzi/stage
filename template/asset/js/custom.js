/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// CUSTOM
//======================================================================

$(document).ready(function(){
    activateSearchIcon();
});

function activateSearchIcon() {
    $('.header-search-icon').click(function () {
        $('.header-search-input').focus();
    });
}
