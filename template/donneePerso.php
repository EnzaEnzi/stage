<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : donneePerso.php
// ceci est le formulaire qui permet au client de changer son mot de passe
//======================================================================


 ?>
<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mes données personnelles</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Changer mot de passe
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> form_password -->
                <div id="form_password" class="col-lg-9 col-md-12 col-12">
                    <p>Note: Le mot de passe doit contenir au moins 6 caractères.</p>
                    <div id="box-password">
                        <form action="\" name="changePassword" method="post">
                            <div>
                                <label for="password_old">Mot de passe actuel :</label>
                                <input id="password_old" class="case" name="password_old" type="password">
                                <div class="clear"></div>
                            </div>
                            <div>
                                <label for="password_new">Nouveau mot de passe :</label>
                                <input id="password_new" class="case" name="password_new" type="password">
                                <div class="clear"></div>
                            </div>
                            <div>
                                <label for="password_confirm">Confirmer le mot de passe :</label>
                                <input id="password_confirm" class="case" name="password_confirm" type="password">
                                <div class="clear"></div>
                            </div>
                            <div>
                                <input class="btn btn-success" name="save" value="Enregistrer" type="submit">
                            </div>
                        </form>
                    </div>
                </div>
                <!-- fin -> form_password -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
