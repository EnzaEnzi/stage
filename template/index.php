<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : index.php
// ceci est la page d'accueil du site
//======================================================================


 ?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> slider -->
    <section id="slider" class="no-visible-sl">
        <span id="air"></span>
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <!-- 1er slide -> debut -->
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="asset/img/slide.jpg" alt="Délice en sachet">
                        <div class="carousel-caption">
                            <div class="jumbotron">
                                <h1 class="display-3">Délices en sachet</h1>
                                <p class="lead my-5">
                                    confiseries artisanales et spécialités belges,
                                    produit de terroir comme le vrai cuberdon original, ...
                                </p>
                                <p class="lead my-5">
                                    <a class="btn btn-lg" href="listProduit.php" role="button">
                                        Découvrez nos produits
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- 1er slide -> fin -->
                    <!-- 2e slide -> debut -->
                    <div class="carousel-item">
                        <img class="d-block w-100" src="asset/img/slide.jpg" alt="Délice en sachet">
                        <div class="carousel-caption">
                            <div class="jumbotron">
                                <h1 class="display-3">Délices XL</h1>
                                <p class="lead my-5">
                                    spécialités artisanales belges, PRODUITS DE TERROIR LARDS ( guimauve ),
                                    CUBERDONS, SOURIS, PATE DE FRUITS, ...
                                </p>
                                <p class="lead my-5">
                                    <a class="btn btn-lg" href="listProduit.php" role="button">
                                        Découvrez nos produits
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- 2e slide -> fin -->
                    <!-- 3e slide -> debut -->
                    <div class="carousel-item">
                        <img class="d-block w-100" src="asset/img/slide.jpg" alt="Délice en sachet">
                        <div class="carousel-caption">
                            <div class="jumbotron">
                                <h1 class="display-3">Délice Belge</h1>
                                <p class="lead my-5">
                                    Depuis 1895, Gicopa fabrique de façon traditionnelle ,
                                    une gamme très étendue de bonbons acidulés de qualité...
                                    <p class="lead my-5">
                                        <a class="btn btn-lg" href="listProduit.php" role="button">
                                            Découvrez nos produits
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- 3e slide -> fin -->
                        <!-- 4e slide -> debut -->
                        <div class="carousel-item">
                            <img class="d-block w-100" src="asset/img/slide.jpg" alt="Délice en sachet">
                            <div class="carousel-caption">
                                <div class="jumbotron">
                                    <h1 class="display-3">Délice de Saison</h1>
                                    <p class="lead my-5">
                                        A chaque saison ça gourmandise, découvre
                                        encore plus de spécialités belges
                                    </p>
                                    <p class="lead my-5">
                                        <a class="btn btn-lg" href="listProduit.php" role="button">
                                            Découvrez nos produits
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- 4e slide -> fin -->
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>
        <!-- fin -> slider -->

        <!-- debut -> vignette -->
        <section id="vignette">
            <div class="container">
                <div class="row">
                    <!-- 1er vignette -> debut -->
                    <div class="col-lg-6 col-md-6 space">
                        <a class="card" href="listProduit.php">
                            <img class="d-block" src="asset/img/gommette.jpg" alt="Délice en sachet" >
                            <div>
                                <h1>Délice en sachet</h1>
                                <p class="btn btn-lg" role="button">Découvrez nos produits</p>
                            </div>
                            <img class="plus" src="asset/img/rectangle.png" alt="" >
                        </a>
                    </div>
                    <!-- 1er vignette -> fin -->
                    <!-- 2e vignette -> debut -->
                    <div class="col-lg-6 col-md-6">
                        <a class="card" href="listProduit.php">
                            <img class="d-block" src="asset/img/gommette.jpg" alt="Délice XL" >
                            <div>
                                <h1>Délice XL</h1>
                                <p class="btn btn-lg" role="button">Découvrez nos produits</p>
                            </div>
                            <img class="plus" src="asset/img/rectangle.png" alt="" >
                        </a>
                    </div>
                    <!-- 2e vignette -> fin -->
                </div>
                <div class="row">
                    <!-- 3e vignette -> debut -->
                    <div class="col-lg-6 col-md-6 space">
                        <a class="card" href="listProduit.php">
                            <img class="d-block img-vedette" src="asset/img/bonbon_gicopa.jpg" alt="Bonbons Gicopa" >
                            <div>
                                <h1>Bonbons Gicopa</h1>
                                <p class="btn btn-lg" role="button">Découvrez nos produits</p>
                            </div>
                            <img class="plus" src="asset/img/rectangle.png" alt="">
                        </a>
                    </div>
                    <!-- 3e vignette -> fin -->
                    <!-- 4e vignette -> debut -->
                    <div class="col-lg-6 col-md-6">
                        <a class="card" href="listProduit.php">
                            <img class="d-block img-vedette" src="asset/img/saison_printemps.jpg" alt="Délice de saison" >
                            <div>
                                <h1>Délice de saison</h1>
                                <p class="btn btn-lg" role="button">Découvrez nos produits</p>
                            </div>
                            <img class="plus" src="asset/img/rectangle.png" alt="" >
                        </a>
                    </div>
                    <!-- 4e vignette -> fin -->
                </div>
            </div>
        </section>
        <!-- fin -> vignette -->
</main>

<?php include 'footer.php'; ?>
