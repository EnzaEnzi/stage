<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : header.php
// ceci est le header de TOUTES les pages
//======================================================================
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Template</title>

    <!-- debut -> css -->

    <!-- Add Material font (Roboto) and Material icon as needed -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- Add Material CSS, replace Bootstrap CSS -->
    <link href="asset/css/material.css" rel="stylesheet" />

    <!-- add plugin CSS -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/flags.min.css" />
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>


    <!-- add custom CSS -->

    <link rel="stylesheet" type="text/css" href="asset/css/style.css">
    <link rel="stylesheet" type="text/css" href="asset/css/header.css">
    <link rel="stylesheet" type="text/css" href="asset/css/menu-mobile.css">
    <link rel="stylesheet" type="text/css" href="asset/css/footer.css">
    <link rel="stylesheet" type="text/css" href="asset/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="asset/css/main.css">
    <link rel="stylesheet" type="text/css" href="asset/css/produit.css">
    <link rel="stylesheet" type="text/css" href="asset/css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="asset/css/connetion.css">
    <link rel="stylesheet" type="text/css" href="asset/css/panier.css">
    <!-- fin -> css -->

</head>
<body>

    <!-- debut -> header -->
    <header id="header">
        <!-- debut -> navtop -->
        <section id="navtop">
            <div class="container">
                <!-- debut -> search -->
                <div class="row navbar-header">
                    <div class="col-lg-4 col-md-4 ml-auto nav navbar-nav search-nav">
                        <div class="search">
                            <img src="asset/img/icon/search.png" alt="">
                            <div class="form-group form-animate-text">
                                <input type="text" class="form-text" required>
                                <span class="bar"></span>
                                <label class="label-search">Recherche </label>
                            </div>
                        </div>
                    </div>
                    <!-- fin -> search -->

                    <!-- debut -> langue -->
                    <div id="navlang" class="col-lg-1 col-md-2 dropdown no-visible-sl">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            fr</a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="flag flag-fr"></span> Fr</a></li>
                                <li><a href="#"><span class="flag flag-en"></span> En</a></li>
                                <li><a href="#"><span class="flag flag-de"></span> De</a></li>
                                <li><a href="#"><span class="flag flag-nl"></span> Ne</a></li>
                            </ul>

                        </div>
                        <!-- fin -> langue -->
                        <span class="separation no-visible-sl">|</span>
                        <!-- debut -> connection -->
                        <div id="connection" class="col-lg-3 col-md-3">
                            <img src="asset/img/icon/avatar.png" alt="user">
                            <div id="user" class="">
                                <a href="connetion.php" style="color:#fff">
                                    Se connecter
                                </a>
                            </div>
                        </div>
                        <!-- fin -> connection -->
                    </div>
                </div>
            </div>
        </section>
        <!-- fin -> navtop -->

        <!-- debut -> nar-bar -->
        <section id="nav-bar">
            <div class="container">
                <div class="row">
                    <!-- debut -> logo -->
                    <div id="logo" class="col-lg-3 col-md-3 col-6">
                        <a href="index.php">
                            <!-- <img src="asset/img/logo_org.png" alt="c du Belge"> -->
                            <img src="asset/img/logo_v2.png" alt="c du Belge">
                        </a>
                    </div>
                    <!-- fin -> logo -->

                    <!-- debut -> nav-principal -->
                    <div class="col-lg-7 col-md-7 no-visible-sl">
                        <nav id="nav-principal" role="navigation">
                            <ul class="nav nav-tabs justify-content-center">
                                <li class="nav-item">
                                    <a class="nav-link active" href="index.php">
                                        <i id="accueil" class="fa fa-home fa-2x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="listProduit.php">Nos produits</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.php">Nous contacter</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- fin -> nav -->

                    <!-- debut -> panier -->
                    <div id="panier" class="col-lg-2 col-md-2 col-2">
                        <a href="panier.php">
                            <img src="asset/img/icon/shopping-bag.png" alt="Mon panier">
                            <div id="prix" class="">

                            </div>
                        </a>
                    </div>
                    <!-- fin -> panier -->
                </div>
            </div>
        </section>
        <!-- fin -> nav-bar -->
    </header>
    <!-- fin -> header -->
