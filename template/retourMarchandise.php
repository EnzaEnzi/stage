<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// dashboard : retourMarchandise.php
// ceci est la liste des retours de marchandise du client
//======================================================================

 ?>

<?php include 'header.php'; ?>

<main>
    <!-- debut -> vedette -->
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mes retours de marchandise</h1>
        </div>
    </section>
    <!-- fin -> vedette -->

    <!-- debtu -> message -->
    <div id="message" class="container">
        <p>
            Voici la liste des retours produits en cours.
        </p>
    </div>
    <!-- fin -> message -->

    <!-- debut -> dashboard -->
    <section id="dashboard">
        <div class="container">
            <div class="row">
                <?php include 'nav-user.php' ?>

                <!-- debut -> retour_marchandise -->
                <div id="retour_marchandise" class="col-lg-9 col-md-12 col-12">
                    <p>Vous n'avez aucun retour de marchandise créé.</p>
                </div>
                <!-- fin -> retour_marchandise -->
            </div>
        </div>
    </section>
    <!-- fin -> dashboard -->

</main>



<?php include 'footer.php'; ?>
