<?php
/*
Theme Name: cdubelge
Theme URL: http://www.cdubelge.be
Author: Enza Lombardo

Description: Le thème de cdubelge
Version: 2.0
*/

//======================================================================
// font-end : panier-step4.php
// ceci est la page panier d'achat à la 4e étape -> fin
//======================================================================


?>

<?php include 'header.php'; ?>

<main>
    <section id="vedette">
        <div id="titre" class="container">
            <h1>Mon Panier</h1>
        </div>
    </section>

    <!-- debut -> stepper -->
    <section id="step" class="container no-visible-sl">
        <div class="stepper-horiz">
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper done active">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper -->

    <!-- debut -> stepper-mobile -->
    <section id="stepper-mobile" class="container no-visible">
        <div class="stepper-vert">
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Récapitulatif</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Adresse</span>
            </div>
            <div class="stepper">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Paiement</span>
            </div>
            <div class="stepper done active">
                <div class="stepper-icon">
                    <i class="material-icons">check</i>
                </div>
                <span class="stepper-text">Terminé</span>
            </div>
        </div>
    </section>
    <!-- fin -> stepper-mobile -->

    <!-- debut -> box- -->
    <section id="box-final" class="container">
        <div class="row">
            <div class="ml-auto col-lg-8 col-md-8 mr-auto">
                <div id="commande-termine">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <span class="icons icon-emotsmile"></span>
                                Merci pour votre achat !
                            </h4>
                            <div class="card-text">
                                <p>Nous avons reçu votre paiement</p>
                                <p>Montant: <span> 3,90 €</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- debut -> btn-shopping -->
        <div class="btn-shopping">
            <div class="row">
                <div class="col-6">
                    <a href="listProduit.php" class="btn btn-dark btn-retour">
                        Retour à la boutique
                    </a>
                </div>
                <div class="col-6">
                    <a href="dashboard.php" class="btn btn-success btn-continuer">
                        Mon compte
                    </a>
                </div>
            </div>
        </div>
        <!-- fin -> btn-shopping -->

    </section>
    <!-- fin -> box-paiement -->


</main>

<?php include 'footer.php'; ?>
