# Template | cdubelge #

Création d'un nouveau template pour "cdubelge"

Réalisé par **Enza Lombardo**


## Sources ##


|         |Framework utilisé | Plugin utilisé  |
| ------- |:------------------| :----------------|
| CSS     | Material 4.0 (basé sur Bootstrap 4.0)    |  flags & animate |
| JS      | Bootstrap - Material - Jquery - JS       |  main |


### Fonts ###
* Roboto
* Font-awesome

## Réalisation ##

### header ###

##### navtop #####
1. search
    * J'ai utilisé l'animation de miminium + plugin ```main.js```
    * Page de style utilisée : ```header.css``` + ```navtop.css``` -> responsive inclus

2. navlang
    * J'ai utilisé dropdown de material + plugin ```flag.css```
    * Page de style utilisée : ```header.css``` -> responsive inclus

3. user
    * Page de style utilisée : ```heaver.css``` -> responsive inclus

##### nav-bar #####
1. Logo
    * J'ai utilisé pour le moment le logo d'origine inversé (blanc-noir)
    * Page de style utiliseé : ```header.css``` -> responsive inclus

2. nav-principal
    * j'ai utilisé la nav de Material
    * Pour le responsive j'ai utilisé le menu reponsive de Miminium

    *Remarque \
    Dans le responsive il y a un petit problème :disappointed: (voir code ci-dessous)
    -> Le menu ```nav-list tree``` ne se lance pas et les ```li``` se mettent sur une ligne et non en block* \
    :unamused: Pourtant àa il prend bien ```main.js``` pour faire l'animation du menu-mobile

``` code
<!-- debut -> Menu-mobile -->
../..
	<li class="ripple">
        <a class="tree-toggle nav-header">
            <span class="fa fa-list"></span>Nos produits
            <span class="fa-angle-right fa right-arrow text-right"></span>
        </a>
        <ul class="nav nav-list tree">
            <li><a href="#">Délice en sachet</a></li>
            <li><a href="#">Délice XL</a></li>
            <li><a href="#">Bonbons Gicopa</a></li>
            <li><a href="#">Délice de saison</a></li>
        </ul>
    </li>
 ../..
 <!-- fin -> Menu-mobile -->
```

3. panier
    * Page de style utilisée : ```header.css``` -> responsive inclus


### Footer ###

##### Pastille #####

* 5 pastilles (border dotted + border-radius)
* Page de style utilisée : ```footer.css``` -> responsive inclus

##### info #####
1. Paiement
	* j'ai utilisé le systeme de grille pour alligner les images
	* Page de style utilisée : ```footer.css``` -> responsive inclus

2. nav produit footer
	* j'ai utilisé une nav avec un **chevron-right** comme puce
	* Page de style utilisé : ```foorer.css``` -> responsive inclus

3. contact footer
	* j'ai intgéré une google Maps
	* Page de style utilisée : ```foorer.css``` -> responsive inclus

##### social-media #####
* 3 social-media
* Lien *Faceboock* et *Twitter* -> actif
* Page de style utilisée : ```footer.css``` -> responsive inclus
